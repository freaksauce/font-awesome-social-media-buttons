# README #

Using the aptly named Font Awesome (http://fortawesome.github.io/Font-Awesome/) to create easily editable social media buttons. Simply update the style of the li to your liking and update individual id background colours.

In order to update the actual icons I am using a css :before content solution rather than inserting their element in the markup. In order to get the unicode values needed inspect the icon you want on Font Awesome's site, then expand the tag and look at the :before element attributes in the web inspector.

### Font Awesome CSS Social Media Buttons ###

* Converts simple list markup into customisable css buttons using Font Awesome icons
* v0.0.1